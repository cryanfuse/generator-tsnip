# generator-tsnip

> A yeoman generator for Angular Typescript test code (spec) snippets

# Introduction

This project generates snippets of test code based on various known patterns identified in Angular service and component classes under test. It will also parse HTML (templates) and generate protractor test snippets. Supported file extents are .ts and .html.

The resulting snippets are *not* intended to be ready to compile code but a basis for manually pasting into real test code.

The output from tsnip is saved in public/s-imports.ts and s-spec.ts for Services and c-imports/c-spec.ts for Components, and t-page.ts and t-spec.ts for HTML templates.

See CONTRIBUTING.md if contributing to tsnip, and PATTERN.md for code patterns supported by tsnip.

## Pre-requisites

- Node 4.x+
- NPM 3.9+
- nsp for npm scripts (npm install -g nsp)
- typescript (npm install typescript)

As a general requirement the Typescript source you pass into tsnip *should* be clean in terms of 

* Cleanly compiles within your project
* Free of lint errors
* Optimised imports so only required modules are imported (no checks are made to verify an import is actually used)
* Use standard Angular 2/4/5 decorators; @Injectable() for services, @Component() for components

Templates should be complete, valid HTML which include standard data- attributes marking elements under test (see PATTERN.md for details).

## Local sanity check

All going well you should run this

```bash
yo tsnip tests/source/components/example.component.ts
```

and see these files generated:

```bash
public/c-imports.ts
public/c-spec.ts
```

## Installation

First, install [Yeoman](http://yeoman.io) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
```

Check out this generator-tsnip project. In the top directory we need to link the generator into the global node modules:

```bash
cd <generator-tsnip>
npm link
```

You can now run the generator using 'yo tsnip' in your own project. 

In future, once tsnip has been published to the public npmjs.org repository, you'll be able to install using [npm](https://www.npmjs.com/) without having to check out the tsnip source.

```bash
npm install -g generator-tsnip  (Future..)
```

## Usage

Generate test snippets for your service, providing source file argument and options, for example

```bash
cd myproject/src/app
yo tsnip services/travel.service.ts [options]
```

Help guide to options

```bash
yo tsnip --help
```

Options include

* `--imports` generate imports required for a snippet (default: true)
* `--tests` generate test describe/it blocks into a spec file (default: true)
* `--mocks` include mocks for injected services (default: true)
* `--beforeEach` include beforeEach spies and initialisation code (default: true)
* `--url` Route url used with HTML templates to set the page object url
* `--component` Component name used with HTML templates
* `--page` Generate Page class for HTML templates
* `--force` standard yo option to unconditionally overwrite previously generated source (default: prompt for confirmation)

Example

Include import and mock generation and overwrite generated files without prompting me:
 
```yo tsnip tests/source/service/despatch.actions.ts --imports --mocks --force```

Only generate a spec with skeleton describe blocks
 
```yo tsnip tests/source/service/despatch.actions.ts --no-imports --no-mocks --no-beforeEach --force```

## Features

Code analysis of typescript / html files is used to identify typical coding patterns and generate snippets of test code.

Currently supports processing and generation of the following

* Service and Component imports, public methods and parameters
* Mocks for dependant services defined through private constructor parameters
* Describe blocks with let/const declarations
* beforeEach initialisation
* it() blocks with injected dependencies
* Indentify specific use of Redux, Http, Router services
* HTML tags containing data-t- attributes for generating protractor tests

### Limitations

These are a list of features *not* currently provided by tsnip

* Ability to read and parse imported module source
* Extracting templates referenced within components @Component decorator
* Update existing generated code by preserving custom changes
* Generate complete, compilable source

## Futures

* Protractor spec snippets
* Decorators in source to guide tsnip generation. Eg @when(..conditions...) on methods to define test combinations  

## Other projects relevant to tsnip

Typescript parser and AST wrapper

    https://github.com/buehler/node-typescript-parser

JS / ES6 Patterns for implementing the generator 

    https://github.com/ziyasal/design-patterns-and-idioms-in-es6

Example of JS parser and test generator (limited to regex matching):

    https://github.com/sap9433/yoSapy

## License

ISC © [Craig Ryan]()
