
# Test Patterns

Common patterns typically associated with Angular 4 / Typescript Component, Service and template testing are listed here, and supported by tsnip. Each pattern can be generated as a test snippet to be included in a new or existing spec file manually.

# Common to all patterns

Some generation options apply to all patterns.

## Import Patterns

If imports are to be generated (--imports) then all imports in the original source will be included in the generated snippet.

Some standard test imports are also added to this list which tests typically require such as

* @angular/core/testing modules
* @angular/http/testing

## Identified test imports

These are standard third party modules, and their associated test/mock imports. They are included when the original source contains import or code references to commonly used modules such as 

* Redux based code, MockNgRedux, NgReduxTestingModule etc

## Known spies and initialisation

If beforeEach() is to be included (--beforeEach) then certain known spies and initialisation code will be generated, depending on the standard and/or identified modules in use:

* MockNgRedux.mockInstance spy
* MockNxRedux.reset()

# Services patterns

Angular services (and redux Action classes) are identified by an @Injectable() decorator. The service may in turn use other services, provided as constructor parameters.

## Service constructor

The parameters are parsed to identify specific classes of injected services, both unknown (one of yours?) and known commonly used services.

### Known services injected

These include various common service types injected as privates:

* http

### Unknown services injected into services

May generate mock snippets (--mocks). The service import will be resolved with an empty mock class generated at the top of the test.

## Service methods

All public methods generate an it() test case.

If the method contains easily identified code to standard services, appropriate mocks may be set including 

* methods calling this.http.get (or post etc) with begin the it() with mocked responses. (NOTE: not currently possible as the TS parser does not expose method blocks)
* if the method returns Observable<type> then the it() will end with method.subscribe() call with nested expects.

# Component pattern

Components are identified by @Component() decorator which may include selector, templateUrl and styleUrls. 

## Component implements

Standard implementations are recognised include OnInit, OnDestroy and other lifecycle calls.

## Component constructor

Both unknown and known services may be mocked and injected.

## Component Known methods

Some known services such as ngOnInit lifecycle calls will generate their own describe block and in some cases specific describe text such as for ngOnInit this will be 'create <component-name> and call ngOnInit' since the test code will include expect component to be truthy type tests.

### Known services used within components

These include various common service types

* route
* router

### Unknown services used within components

May generate mock snippets.

# Template (HTML) patterns

A clever technique in templates is to define attributes on elements of interest using the 'data-t-' prefix (the 'data-' prefix is specified in the HTML spec for holding custom data) which is used to select element without referring to tag, id or class values. Tsnip uses these attributes to generate protractor test cases for each tag, the logic for these tests dependant on the type of tag

* input tags (input, textarea) result in text sent to the element
* clickable tags (a, button) result in a click call
* display tags (span, div etc) test for getText expectations
* other tags (all remaining tags) are tested for isPresent

## Page object

If a page object is required (--page) then code to select each of the data-t tagged elements is generated.

## Template spec

The spec file contains protractor imports, method to allow selector by data-t and describe blocks for the elements under test. This file is generated if --test is specified (default).

