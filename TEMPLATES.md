
# Template variables

A list of variables set by patterns and referenced in the templates. Refer to XxxxSnippetBuilder.js implementations for comments on which builder set which variables for each generator.

## Service (also handles other injectables such as Action classes)

template.imports            set of imports to generate
template.lets               let/const declarations
template.mocks              mocks for injected services (defined by you)
template.stdMocks           mocks for standard stuff like Http
template.injectDecl         types to be injected into test it()
template.injectParam        injected params (with type)
template.beforeeach {
    imports                 TestBed imports and providers
    providers
}
template.describes {
    name                     name of public methods to generate describes for
    type                     return type of method
    params                   params to be passed into the method under test
}


## Component

template.imports            set of imports to generate
template.lets               let/const declarations
template.mocks              mocks for injected services (defined by you)
template.stdMocks           mocks for standard stuff like Router
template.injectPairs        to generate declarations of fixture injector get calls
template.beforeeach {
    imports                 TestBed imports and providers
    providers
}
template.describes {
    name                     name of public methods to generate describes for
    type                     return type of method
    params                   params to be passed into the method under test
}

## Template (HTML)

template.componentName       component name
template.baseName            the file name, less the .html extent, including '.' delimiter
template.elems {             elements containing 'data-t-' attributes
  tag                        original tag name eg 'div'
  dataTAttr                  the attribute less the 'data-t-', eg 'my-field'
  dataTVar                   name to use as a variable, eg 'myField'
}
template.url                 the url for the page object to use
