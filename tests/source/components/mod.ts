@NgModule({
  imports: [OneModule, TwoModule],
  declarations: [OneComponent],
  exports: [OneComponent],
  providers: [OneService]
})
export class ModModule {}
