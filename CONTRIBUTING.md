# Contributing to tsnip

Contributions to tsnip which introduce new test patterns are welcome.

Firstly, a summary of how to setup at a contributor

# Local development setup

* Follow README.md Pre-requisites section, any manual installs (nsp, typescript, yo etc)
* 'npm install' in this project
* 'npm link' so that 'yo tsnip...' command is available in other projects
* Until the third party typescript-parser has accepted my patch to provide additional node links in the parser, need to manually patch node_modules/typescript-parser.
The diffs to the original parser can be seen here:

    https://github.com/buehler/node-typescript-parser/pull/47/files

To apply the patches use the temporarily checked in ZIP containing required changes. Once 'npm install' has been run, unzip as follows

    $ unzip -o typescript-parse.zip

* You can use local test source dir while developing the generators, or go into your Angular project and run 'yo tsnip' from there. To test locally:

    $ cp <ng-dir>/.../my.component.ts tests/source/components
    $ cp <ng-dir>/.../my.service.ts tests/source/services
    $ yo tsnip tests/source/components/my.component.ts (or services/my.service.ts)

output is saved in public/*.ts

# Current TODO List of features for new Contributors to pick up

* Complete service and component generators (under generators/ dir)
* Write a test suite for the generators
* Implement an rxjs Action spec generator (generators/action/..)
* Implement a Directive generator (include test component in the spec with templates to test directives)
* Finish HTML (template) parsing and spec generation - for protractor, component tests
* Method call parsing was started but needs completion - parse public methods and inspect external function calls made
  in order help test those methods (extra expects, error conditions for it() blocks etc)
* (Important) Get the external typescript-parser patch reviewed and merged, needed by this project

# What constitutes a new pattern?

Patterns in the case of tsnip are generally standard pieces of coding used to implement either partial or full test scenarios.

These may include imports, describe/it declarations, beforeEach blocks of initialisation logic or common mock techniques. Some concrete examples of real world examples might be:

An angular 4 (or 5) service which makes a HTTP RESTful GET call to retrieve a list of customers follows a typical pattern when it comes to structuring test cases, including how to mock backend services, standard test modules to import and defining mocked responses.

A component which initiates a Redux action to load a list of orders may adopt common patterns for mocking Observables, store state changes and faking async behaviour.

Patterns supported by tsnip are identified in PATTERNS.md

# Tsnip Design

The generator uses a standard yeoman structure. The top level directory contains

* **generators** directory containing all tsnip generators
* **parsers** directory contains parsers required by tsnip (typescript parser, html parser etc) 

Each top level directory under __generators__ is a snippet generator, creating it's own set of snippet output files. The generator consists of

* **app** directory drives the entire process for the generator and is the entry point for tsnip (app/index.js).
* **common** contain code shared by all generators such as common patterns, base classes, mapping classes.
* **service** a generator supporting test snippets for Ng4 Service classes
* **component** generator for Ng components

The basic code structure (when implementing new features) incorporates the following design, under the specific generator directory

* A **templates** directory contains __.tpl__ files which are [Ejs](http://ejs.co/) templates for test snippet code generation. The file is combined with a set of variables and saved as snippet code in the `public/` directory.

* An **index.js** implements the yeoman lifecycle methods such as constructor(), writing() etc. This class defines a __template__ object which will be populated by the builder with settings to be passed into the code templates.

* A **patterns** directory contains an implementation of a specific pattern, such as imports, describe blocks etc. A pattern typically provides an apply(template) method that populates the 'template' map passed in, and returns additional settings added to the template which are to be included for the pattern. (a new set of import statements for example).
  
* A **builders** directory implements a builder class for the generator, typically buildSnippet() is called from within the writing() method in __index.js__. The builder uses the various __patterns__ to build a complete snippet and then returns back a string of snippet code, and also populates the __template__ object passed through it.

When the snippet and template map is returned from the builder, the __index.js__ writing() method in each generator directory is invoked to call the standard __copyTpl__ yeoman provided methods for processing the code templates and generating the final snippet code in the `public/` directory.
 
# Tsnip Decorators (currently only concept)

A set of standard decorators may be used to mark parts of the application code and help provide Tsnip with an indication of additional test cases to be generated.

  @TsWhen([
    {param: requestId, values: ['1234', undefined]}
  ])
  getProducts(requestId: string) {
  }

With the above, an it() will be generated for 'should accept a valid requestId' and 'should handle undefined requestId' test cases.

# Notes on publishing the package to NPM

To publish, first register with npm and create a user with password. I've done this with craigryan already.

Locally cd to the project directory. The package.json must at least contain 'name' and 'version' fields. 

To publish first run adduser, or login if the user already exists.

* Add a user:

    $ npm adduser
    Username: craigryan
    Password: password

* Or login
    $ npm login
    ...

To see the user has been added:

    $ npm config ls

Or go to https://www.npmjs.org/~craigryan to verify it exists

To publish on npm:

    $ npm publish

After making changes, need to up the version. Manually update, or just bump the version using npm version:

For bug fixes (no API changes) :

    $ npm version patch
    $ npm publish
    
To add new functionality (backward compatibility preserved):

    $ npm version minor
    $ npm publish
    
Breaking changes, not backward compatible

    $ npm version major
    $ npm publish
