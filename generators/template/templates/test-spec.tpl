
const <%- t.componentName -%>Page = require('./<%- t.baseName -%>.page').default;

describe('given I am on the /<%- t.url -%>/ page', () => {
    let page;

    beforeEach(() => {
        page = new <%- t.componentName -%>Page();
    });

    describe('when the page has loaded', () => {

        it('should display default page elements', () => {
            page.go(true).then(() => {
                expect(page.header.isPresent()).toBeTruthy();
            });
        });

        describe('and expected fields', () => {
            beforeEach((done) => {
            });

            <%_ if (t.elems) { -%>
            <%_ t.elems.forEach((elem) => { -%>
            it('should display <%- elem.dataTVar -%>', () => {
              page.go(true).then(() => {
                expect(page.<%- elem.dataTVar -%>.isPresent()).toBeTruthy();
                <%_ if (t.elemTypes) { -%>
                <%_ if (t.elemTypes.display.includes(elem.dataTAttr)) { -%>
                expect(page.<%- elem.dataTVar -%>.getText()).toEqual('');
                <%_ } else if (t.elemTypes.inputs.includes(elem.dataTAttr)) { -%>
                page.<%- elem.dataTVar -%>.sendKeys('').then(() => {
                    // Input expectation
                });
                <%_ } else if (t.elemTypes.clicks.includes(elem.dataTAttr)) { -%>
                page.<%- elem.dataTVar -%>.click().then(() => {
                    // Click expectation
                });
                <%_ } -%>
                <%_ } -%>
              });
            });
            <%_ }); -%>
            <%_ } -%>

        });
    });
});
