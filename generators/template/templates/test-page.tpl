
import {element, by} from 'protractor';

export default class <%- t.componentName -%>Page {

  static DefaultTimeout = 5000;
  
  constructor() {
    this.initSelectors();
    this.url = '<%- t.url -%>';

    this.body = element(by.tagName('body'));

    <%_ if (t.elems) { -%>
    <%_ t.elems.forEach((elem) => { -%>
    this.<%- elem.dataTVar -%> = this.body.element(by.dataTAttribute('<%- elem.dataTAttr -%>');
    <%_ }); -%>
    <%_ } -%>
  }
 
  go() {
    const EC = protractor.ExpectedConditions;
    browser.get(this.url);
    return browser.wait(EC.visibilityOf(this.body), this.DefaultTimeout).then(() => {
      expect(this.body.isDisplayed()).toBeTruthy();
    });
  }
    
  private initSelectors() {
    by.addLocator('dataTAttribute', function(tAttribute, parentElement) {
      const using = parentElement || document;
      return using.querySelectorAll(`[data-t-${tAttribute}]`);
    });
  }
}
