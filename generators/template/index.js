'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const TemplateSnippetBuilder = require('./builders/TemplateSnippetBuilder');

module.exports = class extends Generator {
  constructor(args, opts) {
	  super(args, opts);

	  // First arg is the HTML parser output
	  this.options = args[0];
	  this.htmlParsed = args[1];
	  this.config = {};
	  this.template = {};
  }

  writing() {
	  const builder = new TemplateSnippetBuilder(this.htmlParsed, this.log, this.options, this.config);
	  builder.buildSnippet(this.template);

	  // Page object
	  if (this.options.page && this.template.elems) {
		  this.fs.copyTpl(
			  this.templatePath('test-page.tpl'),
			  this.destinationPath('public/t-page.ts'), {
				  t: this.template
    	  });
	  }
		    
	  // Spec
	  if (this.options.tests) {
		  this.fs.copyTpl(
				  this.templatePath('test-spec.tpl'),
				  this.destinationPath('public/t-spec.ts'), {
					  t: this.template
			  });
	  }
  }

};
