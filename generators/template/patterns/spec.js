'use strict';

/* A pattern for [Template] Spec class */
class SpecPattern {

  constructor(htmlParsed, log) {
    this.htmlParsed = htmlParsed;
    this.log = log;
  }

  apply(template) {
	  this.log('I am a spec pattern for [Template]');
      const inputs = [];
      const clicks = [];
      const other = [];
      const display = [];
	  if (this.htmlParsed.dataTElements) {
          this.htmlParsed.dataTElements.forEach((elem) => {
              switch (this.inputTagType(elem)) {
              case 'text':
                  inputs.push(elem.dataTAttr);
                  break;
              case 'click':
                  clicks.push(elem.dataTAttr);
                  break;
              case 'display':
                  display.push(elem.dataTAttr);
                  break;
              case 'other':
                  other.push(elem.dataTAttr);
                  break;
              }
          });
      }
      template.elemTypes = {
          inputs: inputs, clicks: clicks, display: display, other: other
      };
  }

  inputTagType(elem) {
      switch (elem.tag) {
      case 'input':
      case 'textarea':
          return 'text';
      case 'button':
      case 'a':
          return 'click';
      case 'span':
      case 'div':
      case 'p':
          return 'display';
      default:
          return 'other';
      }
          
  }
};

module.exports = SpecPattern;
