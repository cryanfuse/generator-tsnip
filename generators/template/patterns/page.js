'use strict';

/* A pattern for [Template] Page class */
class PagePattern {

  constructor(htmlParsed, log) {
	  this.htmlParsed = htmlParsed;
    this.log = log;
  }

  apply(template) {
	  this.log('I am a page pattern for [Template]');
//	  parsed = {
//				componentName: this.componentName,
//				dataTElements: [
//					{tag: 'div', dataTAttr: 'foo-bar', dataTVar: 'fooBar'}
//				]
//		}
	  template.elems = this.htmlParsed.dataTElements;
	  template.componentName = this.htmlParsed.componentName;
  }
};

module.exports = PagePattern;
