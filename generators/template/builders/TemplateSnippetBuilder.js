const SnippetBuilder = require('../../common/SnippetBuilder');
const PagePattern = require('../patterns/page');
const SpecPattern = require('../patterns/spec');

/**
 * Builder for Template test code snippets.
 */
class TemplateSnippetBuilder extends SnippetBuilder {

	constructor(htmlParsed, log, options, buildConfig) {
		super(log, options, buildConfig);
		this.htmlParsed = htmlParsed;
	}

	buildSnippet(template) {
		template.componentName = this.htmlParsed.componentName;
		template.baseName = this.htmlParsed.baseName;
		template.url = this.options.url || '';
		
		if (this.includeTests()) {
			this.buildSpec(template);
		}
		if (this.includePage()) {
			this.buildPage(template);	
		}
	}

	// Template specific methods
	
	buildSpec(template) {
		this.log('- [Template] build spec called');
		const specPattern = new SpecPattern(this.htmlParsed, this.log);
		const patternSnippet = specPattern.apply(template);
	}

	buildPage(template) {
		this.log('- [Template] build page called');
		const pagePattern = new PagePattern(this.htmlParsed, this.log);
		const patternSnippet = pagePattern.apply(template);
	}
}

module.exports = TemplateSnippetBuilder;
