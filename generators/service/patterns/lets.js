'use strict';

/* A pattern for [Service] let/const declarations */
class LetsPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a lets pattern for [Service]');
	  let testLets = [];
	  testLets.push({decl: 'let', name: 'service'});
	  if (template.imports && template.imports.libraries.includes('redux-store')) {
	    testLets.push({decl: 'let', name: 'reduxDispatchSpy'});
	    testLets.push({decl: 'let', name: 'reduxSelectSpy'});
	  }
	  for (let service of template.mocks) {
      testLets.push({decl: 'let', name: service.name, type: service.type});
    }
	  if (template.stdMocks) {
      template.stdMocks.forEach((mock) => {
        switch (mock.type) {
        case 'HttpClient':
          testLets.push({decl: 'let', name: 'httpMock', type: 'HttpTestingController'});
          break;
        case 'Router':
          testLets.push({decl: 'let', name: 'router', type: 'Router'});
        default:
          break;
        }
      });
    }

    template.lets = testLets;
  }
};

module.exports = LetsPattern;
