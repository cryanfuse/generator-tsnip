'use strict';

const TypescriptParserModule = require('typescript-parser');

/* A pattern for [Service] describes */
class DescribesPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a describe pattern for [Service]');
	  let describes = [];
	  let methods = this.tsParsed.classMethods();
	  if (methods) {
		  for (let method of methods) {
			  if (method instanceof TypescriptParserModule.MethodDeclaration) {
			    // console.log('-- method name, type', method.name, method.type);
				  if (this.tsParsed.isPublicMethod(method)) {
					  let params = [];
					  if (method.parameters && method.parameters.length > 0) {
						  for (let param of method.parameters) {
							  params.push({
								  name: param.name,
								  type: param.type
							  });
						  }
					  }
					  // TODO set 'title:' for CRUD method patterns, eg 'successfully update ...'
					  const descObj = {
				      name: method.name,
				      type: method.type, // undefined or Observable<any> etc
				      params: params
            };
					  this.describeText(descObj);

					  describes.push(descObj);
				  }
			  }
		  }
	  }
	  template.describes = describes; 
  }

  describeText(describe) {
    var create = /^(create|new).*/;
    var get = /^(get|fetch|lookup|search|find).*/;
    var update = /^(update).*/;
    var remove = /^(delete|remove).*/;
    let should = 'successfully '
    if (create.test(describe.name)) {
      describe.title = 'POST successfully in call to ' + describe.name;
    } else if (get.test(describe.name)) {
      describe.title = 'GET successfully in call to ' + describe.name;
    } else if (update.test(describe.name)) {
      describe.title = 'PUT successfully in call to ' + describe.name;
    } else if (remove.test(describe.name)) {
      describe.title = 'DELETE successfully in call to ' + describe.name;
    }
  }
};

module.exports = DescribesPattern;
