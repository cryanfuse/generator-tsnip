'use strict';

/* A pattern for [Service] beforeEach spies and initialisation */
class BeforeEachPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a beforeEach pattern for [Service]');
	  let testImports = [];
	  let testProviders = [];
    let testAfterCalls = [];
	  let hasCommonHttp = false;
	  testProviders.push({provide: template.serviceName});
    // Imports include standard imports
	  if (template.stdMocks) {
	    template.stdMocks.forEach((mock) => {
	      // this.log('-- beforeEach mock', mock);
	      switch (mock.type) {
	      case 'HttpClient': testImports.push('HttpClientTestingModule'); hasCommonHttp = true; break;
	      default: break;
	      }
      });
    }
	  if (template.imports.libraries.includes('redux-store')) {
		  testImports.push('NgReduxTestingModule');
	  }
	  // Providers for mostly mocked services this service depends on
	  if (hasCommonHttp) {
	    testAfterCalls.push('httpMock.verify();');
	  }
	  if (template.mocks) {
		  template.mocks.forEach((mock) => {
			  testProviders.push({provide: mock.type, useClass: 'Mock' + mock.type});
		  });
	  }
	  if (template.stdMocks && template.stdMocks.includesType('Router')) {
      testProviders.push({provide: 'Router', useClass: 'MockRouter'});
    }
	  template.beforeeach = {
		  imports: testImports,
		  providers: testProviders
	  };
	  template.aftereach = {
	    calls: testAfterCalls
	  };
  }
};

module.exports = BeforeEachPattern;
