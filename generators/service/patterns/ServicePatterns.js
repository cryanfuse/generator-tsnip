const SnippetPatterns = require('../../common/SnippetPatterns');

/**
 * Manage an ordered set of patterns for generating Service test snippets
 */
class ServicePatterns extends SnippetPatterns {

	constructor(tsParsed, log, options, buildConfig) {
		super(log, options, buildConfig);
		this.tsParsed = tsParsed;
	}

	getAll() {
		return this.patterns;
	}
}

module.exports = ServicePatterns;