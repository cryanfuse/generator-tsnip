
<%# Imports determined to be requiried based on original imports found in the source under test -%>
import {TestBed, inject, fakeAsync} from '@angular/core/testing';

<%_ if (t.libraries && t.libraries.includes('commonhttp')) { -%>
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
<%_ } -%>
<%_ if (t.libraries && t.libraries.includes('redux')) { -%>
import {MockNgRedux, NgReduxTestingModule} from '@angular-redux/store/lib/testing';
<% } -%>

<%# Imports present in the source under test, include these in the test .spec.ts also -%>
<%- t.lines -%>

import {<%- t.serviceName -%>} from './<%- t.fileBase -%>';

<%# not used yet... if (t.local) { -%>
<%# t.local.forEach((import) =>{ -%>
<%#- import -%>
<%# }); -%>
<%# } -%>