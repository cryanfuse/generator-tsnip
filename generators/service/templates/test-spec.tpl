
<%_ if (t.mocks) { -%>
<%_ t.mocks.forEach((mock) =>{ -%>
@Injectable()
class Mock<%- mock.type -%> {
  // mocked methods go here..
}
<%_ }); -%>
<%_ } -%>

<%_ if (t.stdMocks && t.stdMocks.includesType('Router')) { -%>
@Injectable()
class MockRouter {
  navigate(commands: any[], extras?: any) {}
  <%_ if (t.imports.exports.includes('NavigationEnd')) { -%>
  public events = Observable.of( new NavigationEnd(0, 'http://localhost:4200/login', 'http://localhost:4200/login'));
  <%_ } else if (t.imports.exports.includes('NavigationStart')) { -%>
  public events = Observable.of( new NavigationStart(0, 'http://localhost:4200/login', 'http://localhost:4200/login'));
  <%_ } -%>
}
<%_ } -%>

describe('<%- t.serviceName -%>', () => {

<%_ if (t.lets) { -%>
<%_ t.lets.forEach((xlet) => { -%>
  <%- xlet.decl -%> <%- xlet.name -%> <%_ if (xlet.type){ -%>: <%- xlet.type -%><%_ } -%><%_ if (xlet.value){ -%>= <%- xlet.value -%><%_ } -%>;
<%_ }); -%>
<%_ } -%>

<%_ if (t.beforeeach) { -%>
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
      <%_ if (t.beforeeach.providers) { -%>
        <%_ t.beforeeach.providers.forEach((provider) => { -%>
        <%_ if (provider.useClass) { -%>
        { provide: <%- provider.provide -%>, useClass: <%- provider.useClass -%> },
        <%_ } else { -%>
        <%- provider.provide -%>,
        <%_ } -%>
        <%_ }); -%>
      <%_ } -%>
      ] ,
      imports: [
      <%_ if (t.beforeeach.imports) { -%>
        <%_ t.beforeeach.imports.forEach((ximport) => { -%>
        <%- ximport -%>,
        <%_ }); -%>
      <%_ } -%>
      ]
    });

    service = TestBed.get(<%- t.serviceName -%>);

    <%_ if (t.mocks) { -%>
    <%_ t.mocks.forEach((mock) => { -%>
    <%- mock.name -%> = TestBed.get(<%- mock.type -%>);
    <%_ }); -%>
    <%_ } -%>

    <%_ t.stdMocks.forEach((smock) => { -%>
    <%- smock.name -%> = TestBed.get(<%- smock.type -%>);
    <%_ }); -%>
    
    <%_ if (t.imports.libraries.includes('redux-store')) { -%>
    reduxDispatchSpy = spyOn(MockNgRedux.mockInstance, 'dispatch');
    reduxSelectSpy = spyOn(MockNgRedux.mockInstance, 'select');

    MockNgRedux.reset();
    <%_ } -%>
  });
<%_ } -%>

  it('should initialise the <%- t.serviceName -%>', () =>
    expect(service).toBeTruthy();
  });
  
<%_ if (!t.describes) { -%>
  // Method describes go here...
<%_ } else { -%>
  <%_ t.describes.forEach((describe) => { -%>
  describe('and <%- describe.name -%>', () => {
  
    beforeEach(() => {
    });

<%_ if (t.aftereach) { -%>
    afterEach(() => {
      <%_ if (t.aftereach.calls) { -%>
      <%_ t.aftereach.calls.forEach((xcall) => { -%>
        <%- xcall %>
      <%_ }) -%>
      <%_ } -%>
    });
<%_ } -%>

    it('should <%_ if (describe.title) { -%> <%- describe.title -%>' <%_ } else { -%> successfully call <%- describe.name -%>' <%_ } -%>, () =>

      // Given
      <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
      const <%- param.name -%>: <%- param.type -%> = undefined;
        <%_ }); -%>
      <%_ } -%>

      // When
      <% if (describe.type && describe.type !== 'void') { -%>const result: <%- describe.type -%> = <%_ } -%>service.<%- describe.name -%>( 
        <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
        <%- param.name -%>,
        <%_ }); -%>
      );
      <%_ } -%>
      
      // Then

      <%_ if (t.imports && t.imports.libraries.includes('commonhttp')) { -%>
      <%_ var crud=/^(get|fetch|lookup|search|find|create|update|new|delete|remove).*/; -%>
      <%_ if (crud.test(describe.name)) { -%>
      const url = 'http://localhost:8080/';
      const reqBody = {}; 
      const expectedBody = {}; 

      const req = httpMock.expectOne(url);
      req.flush(reqBody);

      expect(connection.request.url).toEqual(url);
      <%_ var get = /^(get|fetch|lookup|search|find).*/; -%>
      <%_ var create = /^(create|new).*/; -%>
      <%_ var update = /^(update).*/; -%>
      <%_ var remove = /^(delete|remove).*/; -%>
      <%_ if (get.test(describe.name)) { -%>
      expect(connection.request.method).toEqual('GET');
      <%_ } -%>
      <%_ if (update.test(describe.name)) { -%>
      expect(connection.request.method).toEqual('PUT');
      <%_ } -%>
      <%_ if (create.test(describe.name)) { -%>
      expect(connection.request.method).toEqual('POST');
      <%_ } -%>
      <%_ if (remove.test(describe.name)) { -%>
      expect(connection.request.method).toEqual('DELETE');
      <%_ } -%>
      // expect(connection.request.headers.keys().length).toEqual(1);
      // expect(connection.request.headers.keys()).toContain('');
      // expect(connection.request.headers.get('')).toBe('');
      expect(req.request.body).toEqual(expectedBody);
      <%_ } -%>
      <%_ } -%>
      
      <%_  if (t.imports && t.imports.libraries.includes('redux-store')) { -%>
      expect(reduxDispatchSpy).toHaveBeenCalled();
      <%_ } -%>
      <%_ if (t.calls && t.calls[describe.name]) { -%>
      <%_ t.calls[describe.name].expects.forEach((expect) => { -%>
      expect(<%- expect[0] -%>).<%- expect[1] -%>;
      <%_ }); -%>
      <%_ } -%>

    }));
  });
  <%_ }); -%>
<%_ } -%>
});
