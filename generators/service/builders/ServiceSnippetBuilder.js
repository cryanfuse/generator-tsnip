const SnippetBuilder = require('../../common/SnippetBuilder');
const ServicePatterns = require('../patterns/ServicePatterns');
const ImportsPattern = require('../../common/patterns/imports');
const ServiceMocksPattern = require('../../common/patterns/serviceMocks');
const MethodCallsPattern = require('../../common/patterns/methodCalls');
const BeforeEachPattern = require('../patterns/beforeEach');
const DescribesPattern = require('../patterns/describes');
const LetsPattern = require('../patterns/lets');

/**
 * Builder for Service test code snippets.
 */
class ServiceSnippetBuilder extends SnippetBuilder {

	constructor(tsParsed, log, options, buildConfig) {
		super(log, options, buildConfig);
		this.tsParsed = tsParsed;
        this.patterns = new ServicePatterns(this.tsParsed, this.log, this.options, this.config);
	}

	buildSnippet(template) {
	  template.fileBase = this.options.fileBase;
		template.serviceName = this.tsParsed.className();
		if (this.includeTests()) {
			this.buildTests(template);
		}
		// sets template.imports
		if (this.includeImports()) {
			this.buildImports(template);	
		}
		// sets template.mocks
		//      template.stdMocks
		if (this.includeMocks()) {
			this.buildMocks(template);
		}
    // sets template.lets
    if (this.includeTests()) {
      this.buildLets(template);
      this.buildMethodCalls(template);
    }
		// sets template.beforeach.imports/.providers
		if (this.includeBeforeEach()) {
			this.buildBeforeEach(template);
		}
//		return this.lines.join("\n");
	}

	/*
	getSnippet() {
		// this.log('- [Service] getSnippet called');
		const allPatterns = this.patterns.getAll();
		for (let p of allPatterns) {
			const patternSnippet = p.apply();
			// this.log('--- pattern snippet:', patternSnippet);
			this.addSnippet(patternSnippet);
		}
		// this.log('- [Service] getSnippet, all lines', this.lines);
		return this.lines.join("\n");
	}
	*/

	// Service specific methods
	
	buildTests(template) {
		this.log('- [Service] build tests called');
		const describesPattern = new DescribesPattern(this.tsParsed, this.log);
		const patternSnippet = describesPattern.apply(template);
	}

	buildImports(template) {
		this.log('- [Service] build imports called');
		const importPattern = new ImportsPattern(this.tsParsed, this.log);
		const patternSnippet = importPattern.apply(template);
//		this.addSnippet(patternSnippet);
		// this.patterns.add();
	}

	buildMocks(template) {
		this.log('- [Service] build mocks called');
		const mocksPattern = new ServiceMocksPattern(this.tsParsed, this.log);
		const patternSnippet = mocksPattern.apply(template);
	}

	buildLets(template) {
		this.log('- [Service] build lets called');
		const letsPattern = new LetsPattern(this.tsParsed, this.log);
		const patternSnippet = letsPattern.apply(template);
	}

	buildMethodCalls(template) {
		this.log('- [Service] build methodCalls called');
		const methodCallsPattern = new MethodCallsPattern(this.tsParsed, this.log);
		const patternSnippet = methodCallsPattern.apply(template);
	}

	buildBeforeEach(template) {
		this.log('- [Service] build beforeEach called');
		const beforeEachPattern = new BeforeEachPattern(this.tsParsed, this.log);
		const patternSnippet = beforeEachPattern.apply(template);
	}

	/*
	addSnippet(slines) {
		const added = this.lines.concat(slines);
		this.log('- [Service] addSnippet', added);
		this.lines = added;
	};
	*/
}

module.exports = ServiceSnippetBuilder;
