'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const ServiceSnippetBuilder = require('./builders/ServiceSnippetBuilder');

module.exports = class extends Generator {
  constructor(args, opts) {
	  super(args, opts);
	  this.log('[Service]: generator');

	  // First arg is the TS parser output
	  this.options = args[0];
	  this.tsParsed = args[1];
	  this.config = {};
	  this.template = {};
  }

  writing() {
	  if (!this.tsParsed.isType('Injectable')) {
		  this.log('[Service]: not a service (no @Injectable() decorator found), skip..');
		  return;
	  }

	  const builder = new ServiceSnippetBuilder(this.tsParsed, this.log, this.options, this.config);
	  builder.buildSnippet(this.template);

	  // Imports
	  if (this.options.imports && this.template.imports) {
		  this.fs.copyTpl(
			  this.templatePath('test-imports.tpl'),
			  this.destinationPath('public/s-imports.ts'), {
				  t: {
					  lines: this.template.imports.lines.join("\n"),
					  libraries: this.template.imports.libraries,
					  fileBase: this.template.fileBase,
					  serviceName: this.template.serviceName
				  }
    	  });
	  }
		    
	  // Spec
	  if (this.options.tests || this.options.mocks || this.options.stdMocks || this.options.beforeEach) {
		  this.fs.copyTpl(
				  this.templatePath('test-spec.tpl'),
				  this.destinationPath('public/s-spec.ts'), {
					  t: this.template
			  });
	  }
  }

};
