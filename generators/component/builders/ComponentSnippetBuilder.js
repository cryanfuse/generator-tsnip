const SnippetBuilder = require('../../common/SnippetBuilder');
const ImportsPattern = require('../../common/patterns/imports');
const MethodCallsPattern = require('../../common/patterns/methodCalls');
const ServiceMocksPattern = require('../../common/patterns/serviceMocks');
const BeforeEachPattern = require('../patterns/beforeEach');
const DescribesPattern = require('../patterns/describes');
const LetsPattern = require('../patterns/lets');
const ObservablesPattern = require('../patterns/observables');

/**
 * Builder for Component test code snippets.
 */
class ComponentSnippetBuilder extends SnippetBuilder {

	constructor(tsParsed, log, options, buildConfig) {
		super(log, options, buildConfig);
		this.tsParsed = tsParsed;
	}

	buildSnippet(template) {
	  template.fileBase = this.options.fileBase;
	  template.componentName = this.tsParsed.className();
		// const pairs = this.tsParsed.classDecoratorPairs();
		// console.log('-- class dec pairs', pairs);
		if (this.includeTests()) {
			this.buildTests(template);
		}
		// sets template.imports
		if (this.includeImports()) {
			this.buildImports(template);	
		}
    // sets template.mocks
    //      template.stdMocks
    //      template.injectDecl
    //      template.injectParam
    if (this.includeMocks()) {
      this.buildMocks(template);
    }
		// sets template.lets, template.subjects, template.calls
		if (this.includeTests()) {
			this.buildLets(template);
			this.buildObservables(template);
			this.buildMethodCalls(template);
		}
		// sets template.beforeach.imports/.providers
		if (this.includeBeforeEach()) {
			this.buildBeforeEach(template);
		}
	}
	
	buildTests(template) {
		this.log('- [Component] build tests called');
		const describesPattern = new DescribesPattern(this.tsParsed, this.log);
		const patternSnippet = describesPattern.apply(template);
	}

	buildImports(template) {
		this.log('- [Component] build imports called');
		const importPattern = new ImportsPattern(this.tsParsed, this.log);
		const patternSnippet = importPattern.apply(template);
	}

	buildMocks(template) {
		this.log('- [Component] build mocks called');
		const mocksPattern = new ServiceMocksPattern(this.tsParsed, this.log);
		const patternSnippet = mocksPattern.apply(template);
	}

	buildLets(template) {
		this.log('- [Component] build lets called');
		const letsPattern = new LetsPattern(this.tsParsed, this.log);
		const patternSnippet = letsPattern.apply(template);
	}

	buildMethodCalls(template) {
		this.log('- [Component] build methodCalls called');
		const methodCallsPattern = new MethodCallsPattern(this.tsParsed, this.log);
		const patternSnippet = methodCallsPattern.apply(template);
	}

	buildObservables(template) {
		this.log('- [Component] build observables called');
		const observablesPattern = new ObservablesPattern(this.tsParsed, this.log);
		const patternSnippet = observablesPattern.apply(template);
	}

	buildBeforeEach(template) {
		this.log('- [Component] build beforeEach called');
		const beforeEachPattern = new BeforeEachPattern(this.tsParsed, this.log);
		const patternSnippet = beforeEachPattern.apply(template);
	}
}

module.exports = ComponentSnippetBuilder;
