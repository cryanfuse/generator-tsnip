'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const ComponentSnippetBuilder = require('./builders/ComponentSnippetBuilder');

module.exports = class extends Generator {
  constructor(args, opts) {
	  super(args, opts);
	  this.options = args[0];
	  this.tsParsed = args[1];
	  this.config = {};
	  this.template = {};
  }

  writing() {
	  if (!this.tsParsed.isType('Component')) {
		  this.log('[Component]: not a component (no @Component() decorator found), skip..');
		  return;
	  }

	  // Debug
	  // this.log('class props', this.tsParsed.classProperties());
	  // this.log('-- class decorator', this.tsParsed.classDecoratorPairs());

	  const builder = new ComponentSnippetBuilder(this.tsParsed, this.log, this.options, this.config);
	  builder.buildSnippet(this.template);

	  // Imports
	  if (this.options.imports && this.template.imports) {
		  this.fs.copyTpl(
			  this.templatePath('test-imports.tpl'),
			  this.destinationPath('public/c-imports.ts'), {
				  t: {
					  lines: this.template.imports.lines.join("\n"),
					  libraries: this.template.imports.libraries,
					  fileBase: this.template.fileBase,
            componentName: this.template.componentName
				  }
    	  });
	  }
		    
	  // Spec
	  if (this.options.tests || this.options.mocks || this.options.stdMocks || this.options.beforeEach) {
		  this.fs.copyTpl(
				  this.templatePath('test-spec.tpl'),
				  this.destinationPath('public/c-spec.ts'), {
					  t: this.template
		  });
	  }
  }

};
