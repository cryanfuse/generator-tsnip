'use strict';

/* A pattern for [Component] beforeEach spies and initialisation */
class BeforeEachPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a beforeEach pattern for [Component]');
	  let testImports = [];
	  let testProviders = [];
	  let testOverrides = [];
	  let hasCommonHttp = false;
	  testProviders.push({provide: template.componentName});
	  // Imports include standard imports
	  if (template.stdMocks) {
		  template.stdMocks.forEach((mock) => {
			  switch (mock) {
			  case 'HttpClient': testImports.push('HttpClientTestingModule'); hasCommonHttp = true; break;
			  default: break;
			  }
		  });
	  }
	  if (template.imports.libraries.includes('redux-store')) {
		  testImports.push('NgReduxTestingModule');
	  }
	  // Providers for mostly mocked services this service depends on
	  if (template.mocks) {
		  template.mocks.forEach((mock) => {
			  testOverrides.push({provide: mock.type, useClass: 'Mock' + mock.type});
		  });
	  }
	  if (template.stdMocks && template.stdMocks.includesType('Router')) {
		  testOverrides.push({provide: 'Router', useClass: 'MockRouter'});
	  }
	  template.beforeeach = {
		  imports: testImports,
		  providers: testProviders,
		  overrides: testOverrides
	  }
  }
};

module.exports = BeforeEachPattern;
