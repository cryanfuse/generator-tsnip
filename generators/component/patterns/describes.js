'use strict';

const TypescriptParserModule = require('typescript-parser');

/* A pattern for [Component] describes */
class DescribesPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a describe pattern for [Component]');
	  let describes = [];
	  let stdDescribes = []; // lifecycle methods, ngOnInit etc
	  let methods = this.tsParsed.classMethods();
	  if (methods) {
		  for (let method of methods) {
			  // this.log('-- method');
			  // this.log(method);
			  if (method instanceof TypescriptParserModule.MethodDeclaration) {
				  if (this.tsParsed.isPublicMethod(method)) {
				  	// Debug for now
				  	// this.tsParsed.methodCalls(method);
					  let params = [];
					  if (method.parameters && method.parameters.length > 0) {
						  for (let param of method.parameters) {
							  params.push({
								  name: param.name,
								  type: param.type
							  });
						  }
					  }
					  let descObj = {
					  		name: method.name,
					  		type: method.type, // undefined or Observable<any> etc
					  		params: params
					  };
					  if (this.knownComponentMethod(this.tsParsed.className(), method, descObj)) {
					    stdDescribes.push(descObj);
					  } else {
					    describes.push(descObj);					    
					  }
				  }
			  }
		  }
	  }
	  template.describes = describes; 
	  template.stdDescribes = stdDescribes; 
  }

  knownComponentMethod(component, method, describe) {
	  switch (method.name) {
	  case 'ngOnInit':
	  	describe.title = 'create ' + component + ' and call ngOnInit';
	  case 'ngOnDestroy':
	  case 'ngOnChanges':
    case 'ngDoCheck':
    case 'ngAfterContentInit':
    case 'ngAfterContentChecked':
    case 'ngAfterViewInit':
    case 'ngAfterViewChecked':
		  return true;
	  default:
		  return false;
	  }
  }
};

module.exports = DescribesPattern;
