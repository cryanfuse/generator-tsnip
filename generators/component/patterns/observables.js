'use strict';

/* A pattern for [Component] observable mocks */
class ObservablesPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am an observables pattern for [Component]');
	  let subjects = [];
	  if (template.imports && template.imports.libraries.includes('redux')) {
		  const properties = this.tsParsed.classProperties();
		  if (properties) {
			  properties.forEach((prop) => {
				  if (this.tsParsed.isPublicField(prop)) {
					  const subjectType = this.getObservableType(prop);
					  if (subjectType) {
						  // this.log('--- observe ', prop.name, subjectType);
						  subjects.push({name: this.subjectName(prop.name), type: subjectType});
					  }
				  }
			  });
		  }
	  }
	  template.subjects = subjects;
  }
  
  // Get the type inside 'Observable<type>'
  getObservableType(prop) {
  	if (prop.type && prop.type.startsWith('Observable<')) {
  		return prop.type.slice(11, (prop.type.length - 1));
  	}
  	return undefined;
  }

  subjectName(name) {
  	if (name.endsWith('$')) {
  		return name.slice(0, name.length - 1);
  	}
  	return name;  
  }
};

module.exports = ObservablesPattern;
