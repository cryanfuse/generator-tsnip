'use strict';

/* A pattern for [Component] let/const declarations */
class LetsPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {
	  this.log('I am a lets pattern for [Component]');
	  let testLets = [];
	  testLets.push({decl: 'let', name: 'component', type: template.componentName });
	  testLets.push({decl: 'let', name: 'fixture', type: 'ComponentFixture<' + template.componentName + '>'});
	  if (template.imports && template.imports.libraries.includes('router')) {
	    testLets.push({decl: 'let', name: 'router'});
	  }
    for (let service of template.mocks) {
      testLets.push({decl: 'let', name: service.name, type: service.type});
    }
	  /* Shouldn't be calling these directly from a component?
	  if (template.imports && template.imports.libraries.includes('redux-store')) {
		  testLets.push({decl: 'let', name: 'reduxDispatchSpy'});
		  testLets.push({decl: 'let', name: 'reduxSelectSpy'});
	  }
	  */
	  template.lets = testLets;
  }
};

module.exports = LetsPattern;
