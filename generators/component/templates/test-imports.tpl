
<%# Imports determined to be requiried based on original imports found in the source under test -%>
import {TestBed, inject, fakeAsync, ComponentFixture} from '@angular/core/testing';
import {DebugElement, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';

<%_ if (t.libraries && t.libraries.includes('commonhttp')) { -%>
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
<%_ } -%>
<%_ if (t.libraries && t.libraries.includes('redux')) { -%>
import {MockNgRedux, NgReduxTestingModule} from '@angular-redux/store/lib/testing';
<%_ } -%>

<%# Imports present in the source under test, include these in the test .spec.ts also -%>
<%- t.lines -%>

import {<%- t.componentName -%>} from './<%- t.fileBase -%>';