
<%_ if (t.mocks) { -%>
<%_ t.mocks.forEach((mock) =>{ -%>
@Injectable()
class Mock<%- mock.type -%> {
  // mocked methods go here..
}
<%_ }); -%>
<%_ } -%>

<%_ if (t.stdMocks && t.stdMocks.includesType('Router')) { -%>
@Injectable()
class MockRouter {
  navigate(commands: any[], extras?: any) {}
}
<%_ } -%>

describe('<%- t.componentName -%>', () => {

<%_ if (t.lets) { -%>
<%_ t.lets.forEach((xlet) => { -%>
  <%- xlet.decl -%> <%- xlet.name -%> <%_ if (xlet.type) { -%>: <%- xlet.type -%><%_ } -%><%_ if (xlet.value){ -%>= <%- xlet.value -%><%_ } -%>;
<%_ }); -%>
<%_ } -%>

<%_ if (t.beforeeach) { -%>
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        <%- t.componentName -%>
      ],
      imports: [
       <%_ if (t.beforeeach.imports) { -%>
        <%_ t.beforeeach.imports.forEach((ximport) => { -%>
        <%- ximport -%>,
        <%_ }); -%>
      <%_ } -%>
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }) <%_ if (!t.beforeeach.overrides) { -%>;<%_ } -%>
    <%_ if (t.beforeeach.overrides) { -%>

    .overrideComponent(<%- t.componentName -%>, {
      set: {
        providers: [
        <%_ t.beforeeach.overrides.forEach((override) => { -%>
          {provide: <%- override.provide -%>, useClass: <%- override.useClass -%>},
        <%_ }) -%>
        ]
      }
    });
    <%_ } -%>
    <%_ if (t.imports.libraries.includes('redux-store')) { -%>
    MockNgRedux.reset();
    <%_ } -%>
  }));
   
<%_ } -%>

<%_ if (t.beforeeach) { -%>
  beforeEach(() => {
    fixture = TestBed.createComponent(<%- t.componentName -%>);
    component = fixture.componentInstance;
    fixture.detectChanges();

    <%_ if (t.stdMocks && t.stdMocks.includesType('Router')) { -%>
    spyOn(router, 'navigate');

    <%_ t.stdMocks.forEach((smock) => { -%>
    <%- smock.name -%> = fixture.debugElement.injector.get(<%- smock.type -%>);
    <%_ }); -%>
    
    <%_ } -%>

    <%_ if (t.mocks) { -%>
    <%_ t.mocks.forEach((mock) => { -%>
    <%- mock.name -%> = fixture.debugElement.injector.get(<%- mock.type -%>);
    <%_ }); -%>
    <%_ } -%>

  });
<%_ } -%>

<%_ if (!t.describes) { -%>
  // Method describes go here...
<%_ } -%>
<%_ if (t.stdDescribes) { -%>
  <%_ t.stdDescribes.forEach((describe) => { -%>
  describe('and <%_ if (describe.title) { -%> <%- describe.title -%>' <%_ } else { -%> <%- describe.name -%>' <%_ } -%>, () => {

    beforeEach(() => {
    });

    it('should successfully call <%- describe.name -%>', () => {
      // Given
      <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
      const <%- param.name -%>: <%- param.type -%> = undefined;
        <%_ }); -%>
      <%_ } -%>

      // When
      <% if (describe.type) { -%>const result: <%- describe.type -%> = <%_ } -%> component.<%- describe.name -%>( 
        <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
        <%- param.name -%>,
        <%_ }); -%>
      );
      <%_ } -%>
      // Then
    });
  });
  <%_ }); -%>
<%_ } -%>

<%_ if (t.describes) { -%>
  describe('UI component tests', () => {

    <%_ if (t.subjects) { -%>
    <%_ t.subjects.forEach((subject) => { -%>
    let <%- subject.name -%>: Subject<<%- subject.type -%>>;
    <%_ }); -%>
    <%_ } -%>

    beforeEach(() => {
    <%_ if (t.subjects) { -%>
    <%_ t.subjects.forEach((subject) => { -%>
      <%- subject.name -%> = MockNgRedux.getSelectorStub<IAppState, <%- subject.type -%>>('<%- subject.name -%>');
    <%_ }); -%>
    <%_ } -%>
    });

    <%_ t.describes.forEach((describe) => { -%>
    describe('and <%- describe.name -%>', () => {

      beforeEach(() => {
      });
    
      it('should successfully call <%- describe.name -%>', fakeAsync(() => {
        // Given
      <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
        const <%- param.name -%>: <%- param.type -%> = undefined;
        <%_ }); -%>
      <%_ } -%>

        // When
        <% if (describe.type && describe.type !== 'void') { -%>const result: <%- describe.type -%> = <%_ } -%>component.<%- describe.name -%>( 
        <%_ if (describe.params) { -%>
        <%_ describe.params.forEach((param) => { -%>
          <%- param.name -%>,
        <%_ }); -%>
        <%_ } -%>
        );
        // Then      
        <%_ if (t.calls && t.calls[describe.name]) { -%>
        <%_ t.calls[describe.name].expects.forEach((expect) => { -%>
        expect(<%- expect[0] -%>).<%- expect[1] -%>;
        <%_ }); -%>
        <%_ } -%>
      }));
    });
    <%_ }); -%>
  });
<%_ } -%>
});
