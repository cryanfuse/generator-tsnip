/**
 * Abstract builder for test code snippet builders.
 * 
 * A builder creates Snippets using Patterns identified from the source under test.
 */
class SnippetBuilder {

	constructor(log, options, buildConfig) {
		this.log = log;
		this.options = options;
		this.buildConfig = buildConfig;
	}

	// Setup methods
	
	applyPatterns(patterns) {
		this.patterns = patterns;
	}	

	// Builder methods
	
	// Prepare the builder for generating a complete snippet using the set of available Patterns. 
	buildSnippet(template) {
		throw new Error('Abstract builderSnippet!');
	}

	// Returns the entire snippet as a string, including embedded newlines
	/*
	getSnippet() {
		throw new Error('Abstract getSnippet!');
	}
	*/

	// checks for what to build

	// Typescript snippets
	includeTests() {
		return this.options.tests ? this.options.tests : true;
	}
	includeImports() {
		return this.options.imports;
	}
	includeMocks() {
		return this.options.mocks;
	}
	includeStdMocks() {
		return this.options.stdMocks;
	}
	includeBeforeEach() {
		return this.options.beforeEach;
	}

	// HTML snippets
	includePage() {
		return this.options.page ? this.options.page : true;
	}
}

module.exports = SnippetBuilder;
