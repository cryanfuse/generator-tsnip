/**
 * Map standard imports (such as redux) to an external module identifier used to trigger specific test imports.
 */
const find = require('lodash/find');

const ImportMaps = {
	'@angular/common/http': 'commonhttp',
	'@angular-redux/store': 'redux-store',
	'@angular-redux/.*': 'redux',
    'rxjs/.*': 'redux',
    '@angular/router': 'router'
};

class ImportMapping {

	static addMaps(map, name) {
		Object.keys(ImportMaps)
		.filter(key => {
			return name.match(new RegExp(key));
		}).forEach(key => {
			// console.log('-- map  ' + key + ' to ' + ImportMaps[key]);
			const newKey = ImportMaps[key];
			if (!map.includes(newKey)) {
				map.push(newKey);
			}
		});
	}
}

module.exports.ImportMaps = ImportMaps;
module.exports.ImportMapping = ImportMapping;
