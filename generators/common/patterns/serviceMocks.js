'use strict';

const TypescriptParserModule = require('typescript-parser');
		
// A pattern for [Service] mocks for injected services 
class ServiceMocksPattern {

  constructor(tsParsed, log) {
	  this.tsParsed = tsParsed;
      this.log = log;
  }

  apply(template) {	  
	  this.log('I am a ServiceMocks pattern for [All]');
	  let ctorParams = this.tsParsed.constructorServices();
	  let mocks = [];
	  let stdMocks = [];
	  /*
	   * Mock:        class MockHostService...
	   * Decl:        let hostService: HostService;
	   * TestBed:     {provide: HostService, useClass: MockHostService}
	   * BeforeEach:  hostService = TestBed.get(HostService)
       *   or for component: fixture.debugElement.injector.get(HostService);
	   */
	  if (ctorParams) {
		  for (let param of ctorParams) {
			  if (param instanceof TypescriptParserModule.ParameterDeclaration && !this.tsParsed.isStandardType(param.type)) {
			    const paramDecl = {name: param.name, type: param.type};
				  if (this.unknownService(param.type)) {
				    // console.log('-- common unknown mock', param.name, param.type, param);
					  mocks.push(paramDecl);
				  } else {
				    console.log('-- std known mock', param.name, param.type);
					  stdMocks.push(paramDecl);
				  }
			  }
		  }
	  }
	  template.mocks = mocks;
	  template.stdMocks = stdMocks;
  }
  
  unknownService(paramType) {
	  switch (paramType) {
	  case 'HttpClient':
	  case 'Router':
	  case 'Route':
		  return false;
	  default: 
		  return true;
	  }
  }
};

module.exports = ServiceMocksPattern;
