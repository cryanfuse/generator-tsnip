'use strict';

const ImportMaps = require('../ImportMaps');
const TypescriptParserModule = require('typescript-parser');
const NamedImport1 = require('typescript-parser/imports/NamedImport');
const StringImport = require('typescript-parser/imports/StringImport');
const ExternalModuleImport = require('typescript-parser/imports/ExternalModuleImport');
const NamespaceImport = require('typescript-parser/imports/NamespaceImport');

/* A pattern for [Service] imports */
class ImportsPattern {

    constructor(tsParsed, log) {
    	this.tsParsed = tsParsed;
    	this.log = log;
    }

    apply(template) {
    	const originalImports = this.tsParsed.originalImports();
    	let t = [];
    	let mods = [];
    	let exports = [];
    	let nextExports = [];
    	for (let imp of originalImports) {
    		if (imp instanceof NamedImport1.NamedImport) {
    			this.namedImport(imp, t, mods, nextExports);
    		} else if (imp instanceof StringImport.StringImport) {
    			this.stringImport(imp, t, mods);
    		} else if (imp instanceof ExternalModuleImport.ExternalModuleImport) {
    			this.externalModuleImport(imp, t, mods);
    		} else if (imp instanceof NamespaceImport.NamespaceImport) {
    			this.namespaceImport(imp, t, mods);
    		} else {
    		  // console.log('-- unknown import type', imp);  
    		}
    		exports = exports.concat(nextExports);
    		nextExports = [];
    	}
    	template.imports = {
    			libraries: mods,
    			exports: exports,
    			lines: t
    	};
    }
    
    // import {a, b} from 'foo'
    namedImport(imp, t, mods, exports) {
    	const mod = imp.libraryName;
    	const specs = this.specifiers(imp.specifiers, exports);
    	if (!imp.defaultAlias) {
    		t.push('import ' + specs + ' from \'' + mod + '\';');
    	} else {
    		if (specs && specs !== '{}') {
    			// TBC not quite right if 'default as <defaultAlias>'..
        		t.push('import ' + imp.defaultAlias + ', ' + specs + ' from \'' + mod + '\';');
    		} else {
        		t.push('import ' + imp.defaultAlias + ' from \'' + mod + '\';');
    		}
    	}
    	ImportMaps.ImportMapping.addMaps(mods, mod);
    }

    // Import a whole NS, eg import * as foobar from 'foobar';
    namespaceImport(imp, t, mods) {
    	const mod = imp.libraryName;
    	if (imp.alias) {
    		t.push('import * as ' + imp.alias + ' from \'' + mod + '\';');
    	} else {
    		t.push('import * from \'' + mod + '\';');
    	}
    	ImportMaps.ImportMapping.addMaps(mods, mod);
    }

    // Eg import foobar = require('foobar')
    externalModuleImport(imp, t, mods) {
    	const mod = imp.libraryName;
    	t.push('import ' + imp.alias + ' = require(\'' + mod + '\');');
    	ImportMaps.ImportMapping.addMaps(mods, mod);
    }

    // Eg import 'rxjs/stuff';
    stringImport(imp, t, mods) {
    	const mod = imp.libraryName;
    	t.push('import \'' + mod + '\';');
    	ImportMaps.ImportMapping.addMaps(mods, mod);
    }
    
    specifiers(specs, symSpecs, braces = true) {
    	for (let spec of specs) {
    		symSpecs.push(this.specifier(spec));
    	}
    	const syms = symSpecs.join(', ');
//    	console.log('-- specifiers ', syms);
    	if (braces) {
    		return '{' + syms + '}';
    	}
    	return syms;
    }

    specifier(spec) {
    	if (spec.alias) {
    		return spec.specifier + ' as ' + spec.alias;
    	}
    	return spec.specifier;
    }
};

module.exports = ImportsPattern;
