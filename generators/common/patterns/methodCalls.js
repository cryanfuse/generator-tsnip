'use strict';

const MethodCallMaps = require('../MethodCallMaps');
const TypescriptParserModule = require('typescript-parser');

/* A pattern for [Service] imports */
class MethodCallsPattern {

    constructor(tsParsed, log) {
    	this.tsParsed = tsParsed;
    	this.log = log;
    }

    apply(template) {
    	this.log('I am a MethodCallsMock pattern for [All]');
    	const methods = this.tsParsed.classMethods();
    	let t = [];
    	template.calls = {};
  	    if (methods) {
    		  for (let method of methods) {
    			  if (method instanceof TypescriptParserModule.MethodDeclaration) {
    				  if (this.tsParsed.isPublicMethod(method)) {
    				  	const calls = this.tsParsed.methodCalls(method);
    				  	const expectsArray = this.methodExpects(calls);
    				  	// console.log('-- method call mapped for ', method);
    				  	template.calls[method.name] = {
    				  			expects: expectsArray
    				  	};
    				  }
    			  }
    		  }
  	    }
//  	  console.log('-- all calls', template.calls);
    }
    
    methodExpects(calls) {
    	let mods = [];
    	calls.forEach(call => {
    		MethodCallMaps.MethodCallMapping.addMaps(mods, call);
    	});
    	return mods;
    }
};

module.exports = MethodCallsPattern;
