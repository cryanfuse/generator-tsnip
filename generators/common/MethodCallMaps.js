/**
 * Map standard method calls (such as http.get) to a key used to generate expect clauses.
 */
const find = require('lodash/find');

const MethodCallMaps = {
	'this.http.get': [
		'http.get', "toHaveBeenCalledWith('')"
	],
	'this.http.put': [
		'http.put', 'toHaveBeenCalled()'
	],
	'this.http.delete': [
		'http.delete', 'toHaveBeenCalled()'
	],
	'this.http.post': [
		'http.post', "toHaveBeenCalledWith('')"
	],
	'this.router.navigate': [
		'router.navigate', "toHaveBeenCalledWith('/')"
	]
};

class MethodCallMapping {

	static addMaps(map, name) {
		// console.log('-- add call maps for ' + name);
		Object.keys(MethodCallMaps)
		.filter(key => {
			return name.match(new RegExp(key));
		}).forEach(key => {
//			console.log('-- mapped  ' + key + ' to ' + MethodCallMaps[key]);
			const newKey = MethodCallMaps[key];
			if (!map.includes(newKey)) {
				map.push(newKey);
			}
		});
	}
}

module.exports.MethodCallMaps = MethodCallMaps;
module.exports.MethodCallMapping = MethodCallMapping;
