/**
 * Abstract pattern for generating familiar test code snippets.
 * 
 * A Pattern is applied by a Builder when generating test snippets.
 */
class SnippetPatterns {

	constructor(log, options, buildConfig) {
		this.log = log;
		this.options = options;
		this.buildConfig = buildConfig;
		this.patterns = [];
	}

	add(pattern) {
		this.patterns.push(pattern);
	}

	getAll() {
		throw new Error('Abstract pattern getAll!');
	}
}

module.exports = SnippetPatterns;
