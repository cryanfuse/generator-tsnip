'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const TsParser = require('../../parsers/typescript/SourceParser.js');
const TsParsed = require('../../parsers/typescript/TsParsed.js');
const HtmlParser = require('../../parsers/html/HtmlParser.js');

module.exports = class extends Generator {
  constructor(args, opts) {
  	super(args, opts);
  
  	this.argument('sourcefile', {
  		desc: 'The path to the source file (TS, html etc) under test',
  		type: String,
  		required: true
  	});

  	/* 
  	 * CLI options, the format is not obvious from yo docs. If defining a boolean option then no value is specified,
  	 * such as '--imports=false', instead two options without values are recognised:
  	 *    --imports     true/generate imports
  	 *    --no-imports  false/don't generate imports
  	 */
  	this.option('tests', {
  		desc: 'Include test describe/it in snippets', type: Boolean, default: true
  	});
  	this.option('imports', {
  		desc: 'Include imports from snippets', type: Boolean, default: true
  	});
  	this.option('mocks', {
  		desc: 'Include mocks for unknown services', type: Boolean, default: true
  	});
  	this.option('stdMocks', {
  		desc: 'Include mocks for standard services (eg http) identified', type: Boolean, default: true
  	});
  	this.option('beforeEach', {
  		desc: 'Include beforeEach with spies and initialisation code', type: Boolean, default: true
  	});
  	this.option('page', {
  		desc: 'Include page snippets from HTML templates', type: Boolean, default: true
  	});
  	this.option('url', {
  		desc: 'Route url used page snippets (HTML templates)', type: String
  	});
  	this.option('component', {
  		desc: 'Component name (HTML templates)', type: String
  	});
  
  	this.builderConfig = {
  		ts: false,
  		html: false
  	};
  }

  initializing() {
	  const supportedExtensions = ['ts', 'html'];
	  const fileExtent = this.options.sourcefile.substr(this.options.sourcefile.lastIndexOf('.') + 1).toLowerCase();
	  const filePath = this.options.sourcefile.substr(0, this.options.sourcefile.lastIndexOf('.')); 
    const pathEnd = this.options.sourcefile.lastIndexOf('/');
	  const fileName = pathEnd === -1 ? filePath : filePath.substr(pathEnd + 1); 
	  this.options.fileBase = fileName;
	  this.isSupportedFileType = fileExtent.length > 0 && supportedExtensions.indexOf(fileExtent) != -1;
	  if (this.isSupportedFileType) {
		  if (fileExtent === 'ts') {
			  this.builderConfig.ts = true;
		  } else {
			  this.builderConfig.html = true;
		  }
	  }
	  // Allow easier lookups in templates
	  // - find by type in [{name: x, type: y}, ..]
	  Array.prototype.includesType = function(typeName) {
	    for (var i in this) {
	      if (this[i].type === typeName) {
	        return true;
	      }
	    }
	    return false;
	  };
  }

  prompting() {
  	this.log(yosay(
    	'Welcome to the ' + chalk.red('generator-tsnip') + ' Typescript test snippet generator!'
    ));
  
  	const prompts = [ // { type: 'input', name: 'sourceName', message: 'Path to source .ts used to generate test snippet? ' }
  	];
  
    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  default() {

	  if (this.builderConfig.ts) {
		  const parser = new TsParser(this.log);
		  parser.parseFile(this.options.sourcefile, '.').then((parsed) => {
			  const tsParsed = new TsParsed(parsed); 
	
			  // Debug:
			  tsParsed.classDecoratorPairs();
			  // parser.displayComponentType();

			  // -- TS has been parsed, initiate generators --
	
			  this.composeWith(require.resolve('../service'), {
				  arguments: [this.options, tsParsed]
			  });
	
			  this.composeWith(require.resolve('../component'), {
				  arguments: [this.options, tsParsed]
			  });
		  },
		  (err) => {
			  this.log(err);
			  this.log('Parse error, try again.. ', err);
		  });
	  } else if (this.builderConfig.html) {

	  	const parser = new HtmlParser(this.log);
	  	parser.parseFile(this.options.sourcefile, this.options.component).then((htmlParsed) => {
			  this.composeWith(require.resolve('../template'), {
				  arguments: [this.options, htmlParsed]
			  });
	  	});
	  }
  }

  writing() {
	  this.log('-> writing() for app');
    /*
    this.fs.copy(
      this.templatePath('dummyfile.txt'),
      this.destinationPath('dummyfile.txt')
    );
    */
  }

  install() {
      // InstallDependencies runs 'npm install' and 'bower install'.
/*
      this.installDependencies({
          skipInstall: true, // this.options['skip-install'],
          skipMessage: true,
          bower: false
      });
*/
  }
};
