'use strict';

const camelCase = require('lodash/camelCase');
const kebabCase = require('lodash/kebabCase');
const upperFirst = require('lodash/upperFirst');
const findIndex = require('lodash/findIndex');

const parse5 = require('parse5');
const fs = require('fs');
const path = require('path');

class HtmlParser {
	constructor(log) {
		this.log = log;
		this.htmlSourceFile = undefined;
		this.componentName = undefined; 
		this.baseName = undefined; 
	}

	parseFile(sourceFile, componentName) {
		this.setNaming(sourceFile, componentName);
		const parsed = {
				baseName: this.baseName,
				componentName: this.componentName,
				dataTElements: []
		};
		const file = fs.createReadStream(this.htmlSourceFile, { encoding: 'utf-8' });
		const parser = new parse5.SAXParser();
		
		fs.exists(this.htmlSourceFile, (exists) => {
			if (!exists) {
				this.log('HTML file does not exist', this.htmlSourceFile);
				return;
			}
		});

		parser.on('startTag', (name, attrs) => {
			this.testTag(parsed.dataTElements, name, attrs);
		});

		file.pipe(parser);
		
		return new Promise((resolve, reject) => {
			parser.on('end', () => {
				resolve(parsed);
			});
			parser.on('error', () => {
				reject();
			});
		});
	}

	setNaming(sourceFile, componentName){
		this.htmlSourceFile = sourceFile;
		if (componentName) {
			this.componentName = componentName;
			this.baseName = kebabCase(componentName).replace('-', '.');
		} else {
			// Use the sourceFile name (path/dir/aaa-bbbb.html -> AaaBbbb)
			this.baseName = path.basename(sourceFile, '.html');
			this.componentName = upperFirst(camelCase(this.baseName));
		}
	}

	testTag(data, tag, attrs) {
	  /* tag: div, 
	   * attrs: 
	   *   [ { name: 'class', value: 'row-line1 recipient-name primary--strong' },
 		 *     { name: 'data-t-recipient-name-element', value: '' } ]
		 */
		if (attrs) {
			attrs.forEach((attr) => {
				if (attr.name.startsWith('data-t-')) {
					const dataTAttr = attr.name.slice(7);  // aaa-bbb
					const dataTVar = camelCase(dataTAttr); // aaaBbb
					if (findIndex(data, function(e) { return e.dataTAttr == dataTAttr; }) === -1) {
						data.push({tag: tag, dataTAttr: dataTAttr, dataTVar: dataTVar});
					}
				}
			});
		}
	}
}

module.exports = HtmlParser;
