'use strict';

const TypescriptParserModule = require('typescript-parser');
const ts = require("typescript");

/* Class decorator, wrapped with convenience methods to access decorator details */
class TsDecorator {

		constructor(classDeclaration) {
			this.decorator = undefined;
			this.pairs = [];
      this.extractDecorator(classDeclaration.tsNode);
      // this.extractProperties(this.decorator);
    };

    decoratorPairs() {
    	if (!this.decorator) {
    		return;
    	}
    	if (!this.pairs) {
    		this.decorator.getChildren().forEach(c => {
    		  // console.log('-- dec pair', c);
    		});
    	}
    	return this.pairs;
    }

//    extractProperties(node) {
//    	if (node.kind && node.kind === ts.SyntaxKind.ObjectLiteralExpression) {
//    		node.getChildren().forEach((c) => {
//    			if (c.kind === ts.SyntaxKind.SyntaxList) {
//    				this.log('--- syntaxlist found');
//    			}
//    		});
//    	} else {
//    		node.getChildren().forEach(c => this.extractProperties(c));	
//    	}
//    }
    
    syntaxKindToName(k) {
    	return ts.SyntaxKind[k];
    }
    
    extractDecorator(node) {
    	if (node.kind === ts.SyntaxKind.SyntaxList) {
    		node.getChildren().some((c) => {
    			if (c.kind === ts.SyntaxKind.Decorator) {
    				this.decorator = c;
    			}
    			return !!this.decorator;
    		});
    	} else {
    		node.getChildren().forEach(c => {
    			if (!this.decorator) {
    				this.extractDecorator(c);
    			}
    		});	
    	}
    }
};

module.exports = TsDecorator;
