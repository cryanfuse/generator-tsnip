'use strict';

const TypescriptParserModule = require('typescript-parser');
const ts = require("typescript");

/* Method block, wrapped with convenience methods to access contained statements */
class TsMethodBody {

    constructor(methodDeclaration) {
      this.calls = undefined;
      this.methodDeclaration = methodDeclaration;
    };

    methodCalls() {
    	if (!this.calls) {
    		this.calls = [];
    		// console.log('-- method calls for [', this.methodDeclaration.name, ']');
    		//this.extractCalls(this.methodDeclaration.tsNode);
    		// TODO: not yet implemented to parse the method body correctly.
    		// this.parseNode(this.methodDeclaration.tsNode);
    	}
    	return this.calls;
    }

    parseNode(node) {
      switch (node.kind) {
          case ts.SyntaxKind.ForStatement:
          case ts.SyntaxKind.ForInStatement:
          case ts.SyntaxKind.WhileStatement:
          case ts.SyntaxKind.DoStatement:
              let iterStatement = /*(ts.IterationStatement)*/ node;
              // if ((<ts.IterationStatement>node).statement.kind !== ts.SyntaxKind.Block) {
              // console.log('-- for/while/do statement', ts.SyntaxKind[iterStatement.statement.kind]);
              break;

          case ts.SyntaxKind.IfStatement:
              let ifStatement = /*(ts.IfStatement)*/ node;
              //if (ifStatement.thenStatement.kind !== ts.SyntaxKind.Block) {
              //}
              // console.log('-- if statement then', ts.SyntaxKind[ifStatement.thenStatement.kind]);
              // console.log('-- if statement else', ts.SyntaxKind[ifStatement.elseStatement.kind]);
              break;
          default:
              // console.log('-- other statement', ts.SyntaxKind[node.kind]);
            break;
//          case ts.SyntaxKind.BinaryExpression:
//              let op = (<ts.BinaryExpression>node).operatorToken.kind;
//              if (op === ts.SyntaxKind.EqualsEqualsToken || op == ts.SyntaxKind.ExclamationEqualsToken) {
//                  report(node, "Use '===' and '!=='.")
//              }
//              break;
      }

      ts.forEachChild(node, (child) => this.parseNode(child));
    }
    
    extractCalls(methodNode) {
      //console.log('-- method body', methodNode.body);
      // this.findBlockNode(methodNode.body);
    	const methodBody = methodNode.body; 
    	if (!methodBody) {
    		return;
    	}
    	const callExpressions = [];
      this.findCallExpressionNodes(methodBody.statements, callExpressions);
    	callExpressions.forEach((c) => {
    		// this.printAllChildren(c);
    		const ids = [];
      	this.extractIdentifiers(c, ids);
    		this.calls.push(ids.join('.'));
    	});
    }

    extractIdentifiers(node, ids) {
    	if (!node) {
    		return;
    	}
    	switch (node.kind) {
    	case ts.SyntaxKind.ThisKeyword:
    		ids.push('this');
    		break;
    	case ts.SyntaxKind.DotToken:
    		break;
    	case ts.SyntaxKind.Identifier:
    		ids.push(node.text);
    		break;
    	case ts.SyntaxKind.OpenParenToken:
    		return; // done
  		default:
  			for (let c of node.getChildren()) {
  				if (c.kind === ts.SyntaxKind.OpenParenToken) {
  					break;
  				}
  				this.extractIdentifiers(c, ids);
  			}
    	}
    }

    findCallExpressionNodes(statements, callExpressions) {
      if (!statements) {
        return;
      }
      // console.log('-- statements', statements);
    	statements.forEach((c) => {
    	  // console.log('-- statement kind', ts.SyntaxKind[c.kind]);
    	  if (c.kind === ts.SyntaxKind.CallExpression) {
    	    // console.log('-- CALL statement');
  				callExpressions.push(c);
        } else if (c.kind === ts.SyntaxKind.Block) {
          // console.log('-- BLOCK statement', c);
          this.findBlockNodes(c, callExpressions);
        } else {
          // console.log('-- ?? statement');
          // this.findCallExpressionNodes(c.statements, callExpressions);
  			}
  		});
    }

    findBlockNodes(blockNode, callExpressions) {
      if (!blockNode) {
        return;
      }
  		for (let c of blockNode.getChildren()) {
  		  if (c.statments) {
  	      this.findCallExpressionNodes(methodBody.statements, callExpressions);
  		  } else if (c.getChildren) {
  		    this.findBlockNodes(c, callExpressions);
  		  }
  		}
    }
};

module.exports = TsMethodBody;
