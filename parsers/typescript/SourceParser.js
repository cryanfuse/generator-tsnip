'use strict';

const TypescriptParserModule = require('typescript-parser');
const tparser = new TypescriptParserModule.TypescriptParser();
const ts = require("typescript");

/* Parse a Typescript source file */
class TsParser {

    constructor(log) {
        this.log = log;
    }

    parseFile(tsSourceFile, tsRoot) {
        return tparser.parseFile(tsSourceFile, tsRoot).then((p) => {
            this.parsed = p;
            return this.parsed;
        });
    }

    // For debugging
    displayComponentType() {
    	this.log('Parsed keys', Object.keys(this.parsed));
    	this.log('resources', this.parsed.resources);
        //this.log('Display Component Types..', this.parsed.declarations);
        if (this.parsed.usages && this.parsed.usages.length > 0) {
            // this.log('Annotated types:');
            for (var usg of this.parsed.usages) {
                // this.log('   ', usg);
            }
        }
        if (this.parsed.declarations) {
            this.log('Decls [' + this.parsed.declarations.length + ']');
            for (var decl of this.parsed.declarations) {
                if (decl instanceof TypescriptParserModule.ClassDeclaration 
                    && decl.isExported === true) {
                	this.log('(exported) Class name: ', decl.name);
                	// this.log(' Class decorator expr: ', decl.tsNode.decorators[0]);
                	// this.log(' Class decorator args: ', decl.tsNode.decorators[0]);
                	this.printAllChildren(decl.tsNode);
                }
            };
        }
    }
    
    printAllChildren(node, depth = 0) {
      this.log(new Array(depth+1).join('----'), this.syntaxKindToName(node.kind), node.pos, node.end, node.text);
      depth++;
      node.getChildren().forEach(c=> this.printAllChildren(c, depth));
    }
    
    syntaxKindToName(k) {
    	return ts.SyntaxKind[k];
    }
};

module.exports = TsParser;
