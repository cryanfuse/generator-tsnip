'use strict';

const TypescriptParserModule = require('typescript-parser');
const NamedImport = TypescriptParserModule.imports;
const TsDecorator = require('./TsDecorator.js');
const TsMethodBody = require('./TsMethodBody.js');

/* Parsed Typescript, wrapped with convenience methods to access the source under test structure */
class TsParsed {

    constructor(parsed) {
      this.parsed = parsed;
      if (this.parsed.declarations) {
        for (var decl of this.parsed.declarations) {
            if (decl instanceof TypescriptParserModule.ClassDeclaration) {
            	this.classDecl = decl;
            }
        };
      }
    };

    // Does the parsed source contain a decorator that matches 'type'
    isType(type) {
  	  const usages = this.parsed ? this.parsed.usages : [];
  	  if (!usages || usages.length === 0) {
  		  return false;
  	  }
  	  for (var u of usages) {
  		  if (u === type) {
  			  return true;
  		  }
  	  }
  	  return false;
  	}

    className() {
    	let name;
    	if (this.classDecl) {
    		name = this.classDecl.name;
    	} 
      return name;
    }

    classDecoratorPairs() {
    	let pairs = [];
    	if (this.classDecl && this.classDecl.tsNode) {
    		const decorator = new TsDecorator(this.classDecl);
    		pairs = decorator.decoratorPairs();
    	}
    	return pairs;
    }

    originalImports() {
    	if (!this.parsed.imports) {
    		return [];
    	}
    	// console.log('-- parsed imports', this.parsed.imports);
    	return this.parsed.imports; // TBC.. iterate, build string array
    };

    constructorServices() {
    	let params;
    	if (this.classDecl && this.classDecl.ctor) {
    		params = this.classDecl.ctor.parameters;
    	}
    	return params;
    }

    classProperties() {
    	let props;
    	if (this.classDecl) {
    		props = this.classDecl.properties;
    	}
    	return props;
    }

    classMethods() {
    	let methods;
    	if (this.classDecl) {
    		methods = this.classDecl.methods;
    	}
    	return methods;
    }

    isPublicMethod(method) {
  	  if (method.visibility === undefined && !method.isAbstract) {
  		  // undefined is public, 0 is private..
  		  return true;
  	  }
  	  return false;
    }

    isPublicField(field) {
  	  if (field.visibility === undefined) {
  		  // undefined is public, 0 is private..
  		  return true;
  	  }
  	  return false;
    }

    isStandardType(type) {
      return this.isBasicType(type) || this.isArrayType(type) || this.isGenericType(type);
    }

    isBasicType(type) {
      return type === 'string' || type === 'number' || type === 'any' || type === 'boolean'
    }

    isArrayType(type) {
      if (type.startsWith('Array') || type.includes('[')) {
        return true;
      }
      return false;
    }

    isGenericType(type) {
      return type.includes('<');
    }

    methodCalls(methodDeclaration) {
    	// console.log('-- tsParsed, method calls ', methodDeclaration.tsNode);
    	const body = new TsMethodBody(methodDeclaration);
    	return body.methodCalls();
    }
};

module.exports = TsParsed;
